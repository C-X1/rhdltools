FROM ubuntu:22.10 as BASE

ENV DEBIAN_FRONTEND noninteractive
ENV TZ Etc/UTC

RUN echo 'Etc/UTC' > /etc/timezone \
    && ln -s /usr/share/zoneinfo/Etc/UTC /etc/localtime \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg \
    tzdata \
    && curl -fsSL https://bazel.build/bazel-release.pub.gpg | gpg --dearmor >bazel-archive-keyring.gpg \
    && mv bazel-archive-keyring.gpg /usr/share/keyrings \
    && echo "deb [arch=amd64 signed-by=/usr/share/keyrings/bazel-archive-keyring.gpg] https://storage.googleapis.com/bazel-apt stable jdk1.8"  \
    | tee /etc/apt/sources.list.d/bazel.list \
    && apt-get update \
    && apt-get install -y --no-install-recommends \
    arduino \
    bazel \
    bison \
    build-essential \
    clang \
    cmake \
    flex \
    gawk \
    git \
    graphviz \
    haskell-platform \
    haskell-stack \
    iverilog \
    libffi-dev \
    libftdi-dev \
    libreadline-dev \
    mercurial \
    pkg-config \
    python3 \
    python3-pip \
    tcl-dev \
    verilator \
    xdot \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

RUN pip3 --no-cache install \
    behave \
    poetry \
    tox \
    pyverilator \
    orderedmultidict

FROM BASE AS INSTALL

WORKDIR /sources


RUN git clone https://github.com/zachjs/sv2v.git sv2v \
    && cd sv2v \
    && make\
    && cp bin/sv2v /usr/local/bin/

RUN git clone https://github.com/chipsalliance/verible.git verible \
    && cd verible \
    && bazel run -c opt :install -- /usr/local/bin

RUN git clone https://github.com/chipsalliance/UHDM.git uhdm \
    && cd uhdm \
    && git submodule update --init --recursive \
    && make -j$(nproc) \
    && make install \
    && make clean

RUN git clone https://github.com/alainmarcel/Surelog.git surelog \
    && cd surelog \
    && git submodule update --init --recursive \
    && make -j$(nproc) \
    && make install \
    && make clean


RUN git clone https://github.com/cliffordwolf/icestorm.git icestorm \
    && cd icestorm \
    && make -j$(nproc) \
    && make install \
    && make clean

RUN git clone https://github.com/cseed/arachne-pnr.git arachne-pnr \
    && cd arachne-pnr \
    && make -j$(nproc) \
    && make install \
    && make clean

RUN git clone https://github.com/cliffordwolf/yosys.git yosys \
    && cd yosys \
    && make -j$(nproc) \
    && make install \
    && make clean



## Just to have it some scripts to setup ice40 - as mentioned on olimex
RUN git clone https://notabug.org/sagaracharya/swarajya.git swarajya
RUN git clone https://github.com/flashrom/flashrom.git flashrom


WORKDIR /
