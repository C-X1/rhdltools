rhdltools
===============================


Development
===========

In the project directory (where the pyproject.toml resides):

Install development environment:

.. code-block:: shell

   poetry install


Enable pre-commit hooks:

.. code-block:: shell

   pre-commit install

Change to the python environment:

.. code-block:: shell

   poetry shell



Linting and Code Formatting with Black
======================================

Disable linter errors:

Create a noqa comment as seen in this example.

.. code-block:: python

    result = subprocess.run(  # noqa: S603,S607
        [test_command],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        shell=True,  # noqa: S602
        cwd=bake_result.project_path,
    )



Disable formatting of black for a code block

.. code-block:: python

   # fmt: off

   var = [
     10, 10, 10,
     20, 20, 20
   ]

   # fmt: on
