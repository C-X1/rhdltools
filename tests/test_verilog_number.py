"""Test VerilogUnit."""

import unittest
from typing import Union

import pytest

from rhdltools.verilog_number import VerilogNumber, VerilogNumberType


class VerilogNumberTest(unittest.TestCase):
    """Test VerilogNumber."""

    def test_reset(self) -> None:
        """Test the reset function."""
        vn = VerilogNumber()
        vn.xzmask = "xxxx"
        vn.vnvalue = 111

        vn.reset()

        assert vn.xzmask is None
        assert vn.vnvalue is None

    def test_any_char_in_string(self) -> None:
        """Test for function checking for any char in a string."""
        assert VerilogNumber._any_char_in_string("adx", "abc")
        assert VerilogNumber._any_char_in_string("adc", "abc")
        assert not VerilogNumber._any_char_in_string("def", "abc")

    def test_convert_hex_octal_to_binary_hex_oct(self) -> None:
        """Test hex/octal to binary string."""
        for convf, base in ([hex, 16], [oct, 8]):
            for input_value in range(0, 2**16):  # noqa: WPS432
                input_string = convf(input_value)[2:]
                result_string = VerilogNumber._convert_hex_octal_to_binary(
                    input_string, base
                )
                result_value = int(result_string, 2)
                assert input_value == result_value, (
                    f"input_value={input_value}, "
                    f"input_string={input_string}, "
                    f"result_string={result_string}"
                )

    def test_convert_hex_octal_to_binary_hex_oct_xy(self) -> None:
        """Test values containing x and z."""
        input_values = (
            ["xzf", 16, "xxxxzzzz1111"],
            ["axzf", 16, "1010xxxxzzzz1111"],
            ["abcxz", 16, "101010111100xxxxzzzz"],
            ["z", 16, "zzzz"],
            ["x", 16, "xxxx"],
            ["x3z7", 8, "xxx011zzz111"],
            ["z", 8, "zzz"],
            ["x", 8, "xxx"],
        )

        for input_string, base, expected_string in input_values:

            result_string = VerilogNumber._convert_hex_octal_to_binary(
                input_string, base
            )
            assert expected_string == result_string, (
                f"input_string={input_string}, ",
                f"result_string={result_string}",
            )

    def test_extend_binary_to_width(self) -> None:
        """Test function for extending a binary string."""
        input_values = (
            ["10010", 8, False, "00010010"],
            ["x10011", 8, False, "xxx10011"],
            ["z10100", 8, False, "zzz10100"],
            ["1x", 8, True, "1111111x"],
            ["1", 4, False, "0001"],
            ["0", 3, False, "000"],
            ["z", 3, False, "zzz"],
            ["x", 2, False, "xx"],
            ["001000", 4, False, "1000"],
        )

        for input_string, width, signed, expected_string in input_values:
            result_string = VerilogNumber._extend_binary_to_width(
                input_string, width, signed
            )
            assert expected_string == result_string, (
                f"input_string={input_string}, " f"result_string={result_string}"
            )

    def test_convert_float(self) -> None:
        """Test conversion function for plain int and float."""
        vn = VerilogNumber()
        etype = VerilogNumberType.floating_point
        result = []

        for input_string in ("1.3", "10.4", "203.3"):
            vn.reset()
            vn._convert_float(input_string)
            result.append(
                (vn.vtype, type(vn.vnvalue), vn.vnvalue == float(input_string))
            )

        assert result == [
            (etype, float, True),
            (etype, float, True),
            (etype, float, True),
        ]

    def test_convert_simple_int(self) -> None:
        """Test conversion function for plain int and float."""
        vn = VerilogNumber()
        etype = VerilogNumberType.integer
        result = []

        for input_string in ("1", "10", "203"):
            vn.reset()
            vn._convert_simple_int(input_string)
            result.append(
                (vn.vtype, type(vn.vnvalue), vn.vnvalue == float(input_string))
            )

        assert result == [
            (etype, int, True),
            (etype, int, True),
            (etype, int, True),
        ]

    @classmethod
    def none_or_bin(
        cls: "VerilogNumber", test_value: Union[None, int, float, str]
    ) -> Union[None, int, float]:
        """
        Return None if none, binary string as int and others directly.

        :param test_value: Value to test
        :returns: Value or converted value
        """
        if test_value is None:
            return None

        if isinstance(test_value, (int, float)):
            return test_value

        return int(test_value, 2)

    def test_from_string(self) -> None:
        """Test generation from string."""
        test_number = 0
        type_xz = VerilogNumberType.xzmask
        type_int = VerilogNumberType.integer
        input_values = [
            ["8'bzx1001", None, "zzzx1001", type_xz],
            ["6'bzx1001", None, "zx1001", type_xz],
            ["4'b1001", 9, None, type_int],
            ["-4s'b0001", -1, None, type_int],
            ["-5s'd10", -10, None, type_int],
            ["4", 4, None, type_int],
            ["4'd10", 10, None, type_int],
            ["x", None, "x", type_xz],
            ["z", None, "z", type_xz],
        ]

        for input_string, vnvalue, xzmask, vtype in input_values:
            inst = VerilogNumber.from_string(input_string)
            assert inst.xzmask == xzmask, test_number
            assert inst.vnvalue == vnvalue, test_number
            assert inst.vtype == vtype, test_number
            test_number += 1

    def test_from_string_exception(self) -> None:
        """Test Exception when not a string."""
        for input_value in ([], 1.2, 1, {}):
            with pytest.raises(ValueError):  # noqa: PT011
                VerilogNumber.from_string(input_value)

    def test_equality_eq(self) -> None:
        """Testing equal check."""
        input_values = (
            ["1", "1"],
            ["-1", "-1"],
            ["8'hxx", "8'h1"],
            ["8'hx1", "8'h1"],
            ["8'bxx", "8'h2"],
            ["8'bzz", "8'hzz"],
            ["8'b1xx1", "8'b01111"],
            ["8'b10101xz1", "8'b101011z1"],
            ["8'b10101xz1", "8'b10101zz1"],
            ["8'b10101xz1", "8'b10101zz1"],
            ["6'b101xz1", "8'b00101zz1"],
            ["4'o1", "1"],
            ["4'o10", "8"],
        )

        for values in input_values:
            vn1 = VerilogNumber.from_string(values[0])
            vn2 = VerilogNumber.from_string(values[1])

            assert vn1 == vn2

    def test_equality_neq(self) -> None:
        """Testing not equal."""
        vn0 = VerilogNumber.from_string("6'b101xz1")
        vn1 = VerilogNumber.from_string("8'b10101zz1")

        result = vn0 != vn1

        assert result


if __name__ == "__main__":
    unittest.main()
