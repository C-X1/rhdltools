"""VerilogUnit is a class to use different verilog unit notations.

Supporting comparisons with x and z.
"""
import re
from enum import Enum
from typing import Union

X_BIT = "x"
Z_BIT = "z"


class VerilogNumberType(str, Enum):  # noqa: WPS600
    """Verilog Number Types."""

    xzmask = "xzmask"
    integer = "integer"
    floating_point = "float"


class VerilogNumber(object):
    """Class to generate and compare pendants to verilog unit notation.

    Especially for the binary notation 8'b01010101
    """

    def __init__(self) -> None:
        """Initialize class."""
        self.signed = False
        self.vnvalue: Union[float, int, None] = None
        self.xzmask = None
        self.vtype = None
        self.z_equal_x = False

    def __repr__(self) -> str:
        """Return string representation for printing.

        :returns: String output for class
        """
        return f"""
        Value: {self.vnvalue}
        Type: {self.vtype}
        xzmask: {self.xzmask}
        """

    def is_xzmask(self) -> bool:
        """Return if type is xzmask.

        :returns: True if type is xzmask, false otherwise
        """
        return self.vtype == VerilogNumberType.xzmask

    #############
    # Operators #
    #############
    def __eq__(self, other: "VerilogNumber") -> bool:
        """Check equality or xz match.

        :param other: The other verilog number class
        :returns: True if it matches
        """
        if self.is_xzmask() and other.is_xzmask():
            return self._xz_mask_compare(self, other)

        if self.is_xzmask() or other.is_xzmask():
            xzmask = None
            vnvalue = None
            if self.vtype == VerilogNumberType.xzmask:
                xzmask = self
                vnvalue = other._value_to_binary()  # noqa: WPS437
            else:
                xzmask = other
                vnvalue = self._value_to_binary()

            return self._xz_mask_compare(xzmask, vnvalue)

        return self.vnvalue == other.vnvalue

    def __neq__(self, other: "VerilogNumber") -> bool:
        """Check in equality or xz match.

        :param other: The other verilog number class
        :returns: True if it matches
        """
        return not self.__eq__(other)

    def reset(self) -> None:
        """Reset to defaults."""
        self.__init__()

    def set_from_string(self, number_string: str) -> None:
        """Set by string.

        :param number_string: The number string
        """
        self.reset()
        conversions = (
            self._convert_simple_int,
            self._convert_float,
            self._x_or_z,
            self._convert_verilog,
        )
        for conversion_func in conversions:
            if self.vtype is not None:
                break

            conversion_func(number_string)

    @staticmethod
    def from_string(number_string: str) -> "VerilogNumber":
        """Convert a string to a verilog number class.

        :param number_string: The number string
        :returns: Verilog number from string
        :raises ValueError: if number_string is not a string
        """
        if not isinstance(number_string, str):
            raise ValueError("Not a string!")

        # pylint: disable=protected-access
        vn = VerilogNumber()
        vn.set_from_string(number_string)
        return vn

    @staticmethod
    def _extend_binary_to_width(value_string: str, width: int, signed: bool) -> str:
        """
        Extend binary string to the given width.

        According to:
        http://web.engr.oregonstate.edu/~traylor/ece474/beamer_lectures/verilog_number_literals.pdf
        0 and 1 are extended to 0
        x is extended with x
        z is extended with z

        :param value_string: The string representation of the value
        :param width: The bit width of the value
        :param signed: Set to true if signed value
        :returns: Expanded string
        """
        if not width:
            return value_string

        first_char = value_string[0].lower()
        extend_char = "0"

        if (first_char in {X_BIT, Z_BIT}) or (signed and first_char == "1"):
            extend_char = first_char

        width_difference = width - len(value_string)
        if width_difference > 0:
            return (extend_char * width_difference) + value_string

        return value_string[-width_difference:]

    @classmethod
    def _xz_mask_compare(  # noqa: WPS210, WPS231
        cls: "VerilogNumber", vn0: "VerilogNumber", vn1: "VerilogNumber"
    ) -> bool:
        """Compare the xz masks of two objects.

        :param vn0: The first object
        :param vn1: The second object
        :returns: true if the masks match, false otherwise
        """
        xzmask0 = vn0.xzmask
        xzmask1 = vn1.xzmask

        lendiff = len(xzmask0) - len(xzmask1)

        if lendiff > 0:
            xzmask1 = cls._extend_binary_to_width(
                xzmask1, len(xzmask1) + lendiff, vn1.signed
            )

        if lendiff < 0:
            xzmask0 = cls._extend_binary_to_width(
                xzmask0, len(xzmask0) - lendiff, vn0.signed
            )

        z_equal_x = vn0.z_equal_x or vn1.z_equal_x
        compare_result = True

        for idx in range(len(xzmask0)):  # noqa: WPS518
            char0 = xzmask0[idx]
            char1 = xzmask1[idx]

            char01 = f"{char0}{char1}"

            xpresent = X_BIT in char01
            zpresent = Z_BIT in char01

            if xpresent or (zpresent and z_equal_x):
                continue

            if char0 != char1:
                compare_result = False
                break

        return compare_result

    def _x_or_z(self, number_string: str) -> None:
        """Convert x, z to xzmask.

        :param number_string: String representation
        """
        number_string = number_string.lower()
        if number_string == X_BIT:
            self.vtype = VerilogNumberType.xzmask
            self.xzmask = X_BIT
        elif number_string == Z_BIT:
            self.vtype = VerilogNumberType.xzmask
            self.xzmask = Z_BIT

    @staticmethod
    def _convert_hex_octal_to_binary(value_string: str, base: int) -> Union[str, int]:
        """Expand hex or octal to binary.

        :param value_string: The string of the value
        :param base: base of the number
        :returns: Conversion result or the original string
        """
        if base not in {8, 16}:
            return value_string

        conversion_size = 4
        if base == 8:
            conversion_size = 3

        convert_result = ""
        for char in value_string:
            if char in "xz":  # Expand
                convert_result += char * conversion_size
            else:
                binary_form = bin(int(char, base))[2:]
                fill_size = 0
                fill_size = conversion_size - len(binary_form)
                convert_result += (fill_size * "0") + (binary_form)

        return convert_result

    def _value_to_binary(self) -> "VerilogNumber":
        if self.vtype == "float":
            raise NotImplementedError("Conversion to binary not implemented for float")

        binary = bin(self.vnvalue).split("b")
        negative = binary[0][0] == "-"
        binary_string = binary[1]

        if negative:
            binary_string = f"1{binary_string}"

        vnum = VerilogNumber()
        vnum.vtype = VerilogNumberType.xzmask
        vnum.xzmask = binary_string

        return vnum

    def _convert_int(  # noqa: WPS211
        self, value_string: str, minus: bool, base: int, width: int, signed: bool
    ) -> None:
        """
        Convert any forms of integer notation.

        :param value_string: The string representation of the value
        :param minus: Set true if value shall be negative
        :param base: Base of number eg 8, 16, 10
        :param width: Bitwidth of the value
        :param signed: Set true if signed
        """
        if base != 10:
            value_string = self._convert_hex_octal_to_binary(value_string, base)
            value_string = self._extend_binary_to_width(value_string, width, signed)
            base = 2
            # Pattern with don't care / high-imp
            if self._any_char_in_string("xz", value_string):
                self.xzmask = value_string
                self.vtype = VerilogNumberType.xzmask
                return
        try:
            self.vnvalue = int(value_string, base)
        except ValueError:
            return

        if minus:
            self.vnvalue *= -1
        self.vtype = VerilogNumberType.integer

    def _convert_verilog_value(  # noqa: WPS211
        self, prefix: str, value_string: str, minus: bool, width: int, signed: bool
    ) -> None:
        """
        Convert verilog numbers depending on type.

        :param prefix: The base prefix (d:10, h:16 ...)
        :param value_string: Value string
        :param minus: Set to true if negative
        :param width: Bit width of value
        :param signed: Set to true if value signed
        """
        self._convert_int(
            value_string,
            minus,
            {
                "d": 10,
                "h": 16,
                "o": 8,
                "b": 2,
            }[prefix],
            width,
            signed,
        )

    # TODO
    #               if minus:
    #                    new_high_mask = self.lowmask  # noqa: E800
    #                    self.lowmask = self.highmask  # noqa: E800
    #                    self.highmask = new_high_mask  # noqa: E800

    def _convert_verilog(self, unit_string: str) -> None:
        match = re.match(
            (
                "(?P<minus>-)?"
                "(?P<width>[0-9]+)?"
                "(?P<signed>[s])?"
                "'"
                "(?P<prefix>[dbho])"
                "(?P<value>[0-9a-fxz_]+)"
            ),
            unit_string.lower().replace("_", ""),
        )

        if match:
            width = int(match.group("width"))
            self.signed = match.group("signed") == "s"
            vnvalue = match.group("value").lower()
            prefix = match.group("prefix").lower()
            minus = match.group("minus") == "-"
            self._convert_verilog_value(
                prefix, vnvalue, minus, width, match.group("minus") == "s"
            )

    def _convert_simple_int(self, number_string: str) -> None:
        """Convert to integer.

        :param number_string: String representation of the number
        """
        try:  # noqa: WPS229
            self.vnvalue = int(number_string)
        except ValueError:
            return
        self.vtype = VerilogNumberType.integer

    def _convert_float(self, number_string: str) -> None:
        """Convert to integer.

        :param number_string: String representation of the number
        """
        try:
            self.vnvalue = float(number_string)
        except ValueError:
            return
        self.vtype = VerilogNumberType.floating_point

    @staticmethod
    def _any_char_in_string(chars: str, string: str) -> bool:
        """Check if any of the chars appear in the string.

        :param chars: A string with all the chars
        :param string: The string to check for the chars
        :returns: True if at least one char of chars is inside string
        """
        for char in chars:
            if char in string:
                return True
        return False
