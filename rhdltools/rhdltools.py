"""Construct and run main application."""

import sys

import structlog
import typer

from rhdltools.cli.app import app

###
# Click imports
###
from rhdltools.cli.example_click import example_click

###
# Typer imports "used" by @app.command() decorator in the file
###
from rhdltools.cli.example_typer import example_typer  # noqa: F401


@app.callback(no_args_is_help=True)
def callback() -> None:
    """Show help when called with no arguments."""


def main() -> None:
    """Construct typer_click interface."""
    log = structlog.get_logger()

    typer_click = typer.main.get_command(app)
    typer_click.add_command(example_click, "example-click")  # type: ignore
    try:
        typer_click()
    except Exception as exception:
        log.fatal("Caught unhandled exception")
        log.exception(exception)
        sys.exit(1)


if __name__ == "__main__":  # pragma: no cover
    main()
