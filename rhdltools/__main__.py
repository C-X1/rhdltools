"""Make rhdltools executable by `python -m rhdltools`."""
from rhdltools import rhdltools

if __name__ == "__main__":  # pragma: no cover
    rhdltools.main()
