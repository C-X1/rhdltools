"""Define application arguments."""


class Completions(object):
    """Implement completion methods."""


class Arguments(object):
    """Implement Arguments for typer."""


class Options(object):
    """Implement Options for typer."""
