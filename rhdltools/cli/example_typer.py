"""Implement typer example."""

from rhdltools.cli.app import app


@app.command()
def example_typer() -> None:
    """Be an example for typer."""
